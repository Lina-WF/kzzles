# coding: utf-8

import pygame
import os
import random
import time


def show_text(size, text_string, color, x, y, rect):                                                    #отрисовка текста
    text = pygame.font.Font('moolbor.ttf', size).render(text_string, 1, color)
    if not x:
        x = width // 2 - text.get_width() // 2
    screen.blit(text, (x, y))
    if rect:
        w, h = text.get_width(), text.get_height()
        pygame.draw.rect(screen, color, (x - 8, y, w + 20, h - 15), 1)
    pygame.display.flip()
    return text


def show_btn_yes(color):                                                                                #отрисовка кнопки Yes
    text = pygame.font.Font('moolbor.ttf', 80).render("Yes", 1, color)
    text_x = width // 2 - text.get_width() // 2 - 50
    text_y = 400
    text_w, text_h = text.get_width(), text.get_height()
    screen.blit(text, (text_x, text_y))
    pygame.draw.rect(screen, (171, 141, 210), (text_x - 5, text_y + 8, text_w + 10, text_h - 45), 1)


def show_btn_no(color):                                                                                #отрисовка кнопки No
    text = pygame.font.Font('moolbor.ttf', 80).render("No", 1, color)
    text_x = width // 2 - text.get_width() // 2 + 60
    text_y = 400
    text_w, text_h = text.get_width(), text.get_height()
    screen.blit(text, (text_x, text_y))
    pygame.draw.rect(screen, (171, 141, 210), (text_x - 5, text_y + 8, text_w + 10, text_h - 45), 1)


def main_window():                                                                                      #отрисовка главного окна
    screen.fill((64, 62, 115))
    show_text(80, "Welcome to Kzless!", (171, 141, 210), None, 100, False)
    show_text(40, "Are     you     ready     to     learn     korean", (171, 141, 210), None, 160, False)
    show_text(40, "and     have     a     lot     of     fun?", (171, 141, 210), None, 190, False)
    show_text(80, "Settings", (171, 141, 210), None, 370, True)
    show_text(80, "Let's play!", (171, 141, 210), None, 650, True)


def settings(lvl, pic_name):                                                                            #отрисовка окна настроек
    screen.fill((64, 62, 115))
    show_text(40, "« Back to menu", (171, 141, 210), 15, 5, True)
    show_text(80, "Alphabet", (171, 141, 210), None, 150, False)
    show_text(40, "vowels", (171, 141, 210), 255, 220, False)
    if lvl[0] == 'False':                                                                               #если гласные не были выбраны изначально
        pygame.draw.rect(screen, (171, 141, 210), (220, 220 + 15, 15, 15), 1)
    else:                                                                                               #если гласные были выбраны изначально
        pygame.draw.rect(screen, (229, 186, 237), (225, 240, 5, 5))
        pygame.draw.rect(screen, (229, 186, 237), (220, 235, 15, 15), 1)
    show_text(40, "consonants", (171, 141, 210), 255, 260, False)
    if lvl[1] == 'False':                                                                               #если согласные не были выбраны изначально
        pygame.draw.rect(screen, (171, 141, 210), (220, 260 + 15, 15, 15), 1)
    else:                                                                                               #если согласные были выбраны изначально
        pygame.draw.rect(screen, (229, 186, 237), (225, 280, 5, 5))
        pygame.draw.rect(screen, (229, 186, 237), (220, 275, 15, 15), 1)
    show_text(40, "diphthongs", (171, 141, 210), 255, 300, False)
    if lvl[2] == 'False':                                                                               #если дифтонги не были выбраны изначально
        pygame.draw.rect(screen, (171, 141, 210), (220, 300 + 15, 15, 15), 1)
    else:                                                                                               #если дифтонги были выбраны изначально
        pygame.draw.rect(screen, (229, 186, 237), (225, 320, 5, 5))
        pygame.draw.rect(screen, (229, 186, 237), (220, 315, 15, 15), 1)
    show_text(80, "Choose the picture", (171, 141, 210), None, 400, False)
    show_text(40, "BTS", (171, 141, 210), 255, 470, False)
    if pic_name != 'bts.jpg':                                                                               #если фон bts не был выбран изначально
        pygame.draw.circle(screen, (171, 141, 210), (227, 470 + 20), 9, 1)
    else:                                                                                                   #если фон bts был выбран изначально
        pygame.draw.circle(screen, (229, 186, 237), (227, 470 + 20), 3)
        pygame.draw.circle(screen, (229, 186, 237), (227, 470 + 20), 9, 1)
    show_text(40, "Stray Kids", (171, 141, 210), 255, 510, False)
    if pic_name != 'skz.jpg':                                                                               #если фон skz не был выбран изначально
        pygame.draw.circle(screen, (171, 141, 210), (227, 510 + 20), 9, 1)
    else:                                                                                                   #если фон skz был выбран изначально
        pygame.draw.circle(screen, (229, 186, 237), (227, 510 + 20), 4)
        pygame.draw.circle(screen, (229, 186, 237), (227, 510 + 20), 9, 1)
    show_text(40, "TXT", (171, 141, 210), 255, 550, False)
    if pic_name != 'txt.jpg':                                                                               #если фон txt не был выбран изначально
        pygame.draw.circle(screen, (171, 141, 210), (227, 550 + 20), 9, 1)
    else:                                                                                                   #если фон txt был выбран изначально
        pygame.draw.circle(screen, (229, 186, 237), (227, 550 + 20), 4)
        pygame.draw.circle(screen, (229, 186, 237), (227, 550 + 20), 9, 1)
    show_text(80, "SAVE", (171, 141, 210), None, 650, True)


class MainPhoto(pygame.sprite.Sprite):                                                                      #реализация спрайта главного фото
    def __init__(self, pic_name):
        super().__init__(main_photo_group)
        fullname = os.path.join('data', pic_name)                                                           #путь к файлу
        im = pygame.image.load(fullname)
        rect = [im.get_width(), im.get_height()]
        min_rect = min(rect)
        delta = min_rect - 500
        im = pygame.transform.scale(im, (rect[0] - delta, rect[1] - delta))                                 #максимальное приближение к размеру 500х500
        self.image = pygame.Surface((500, 500))
        self.image.blit(im, (0, 0, 500, 500))
        self.rect = self.image.get_rect()
        self.rect.x = 50
        self.rect.y = 50
        self.condition = 1                                                                                  #состояние по умолчанию

    def update(self):                                                                                       #тряска перед разломом
        if self.condition:
            self.x = random.randrange(5) - 1
            self.y = random.randrange(5) - 1
            self.rect = self.rect.move(self.x, self.y)
            self.condition = 0
        else:
            self.rect = self.rect.move(-self.x, -self.y)
            self.condition = 1


class PuzzleKorean(pygame.sprite.Sprite):                                                                   #реализация корейской клетки
    def __init__(self, x, y, size, letter, check):
        super().__init__(korean_puzzle_group)
        self.check = check                                                                                  #идентификатор для проверки
        self.image = pygame.Surface((size, size))
        self.image.fill((21, 28, 55))
        font = pygame.font.Font('Koressb_.ttf', size // 2)
        text = font.render(letter, 1, (171, 141, 210))
        text_x = size // 2 - text.get_width() // 2
        text_y = size // 2 - text.get_height() // 2
        self.image.blit(text, (text_x, text_y))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y


class PuzzleEmpty(pygame.sprite.Sprite):                                                                   #реализация пустой клетки
    def __init__(self, x, y, size):
        super().__init__(empty_puzzle_group)
        self.image = pygame.Surface((size, size))
        self.image.fill((18, 25, 50))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y


class PuzzleEnglish(pygame.sprite.Sprite):                                                                   #реализация английской клетки
    def __init__(self, x, y, size, letter):
        super().__init__(english_puzzle_group)
        self.check = letter                                                                                  #идентификатор для проверки
        self.mouse = False                                                                                   #нажата клетка или нет
        self.size = size
        self.image = pygame.Surface((size, size))
        self.image.fill((21, 28, 55))
        font = pygame.font.Font('moolbor.ttf', size // 3 * 2)
        text = font.render(letter, 1, (171, 141, 210))
        text_x = size // 2 - text.get_width() // 2
        text_y = size // 2 - text.get_height() // 2
        self.image.blit(text, (text_x, text_y))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def update(self, window, emppuzzle):
        if window == 'game_rem':
            screen.blit(screen_copy_korean, (0, 0))
            puz = PuzzleEmpty(self.rect.x, self.rect.y, self.size)                                              #создание пустой клетки
            emppuzzle.append(puz)
            empty_puzzle_group.add(puz)
            empty_puzzle_group.draw(screen)
            empty_puzzle_group.draw(screen_copy_korean)
            self.y = random.randint(575, 775 - self.size)                                                       #перемещение английской клетки на случайное место
            self.x = random.randint(25, 575 - self.size)
            self.rect = self.rect.move(self.x - self.rect.x, self.y - self.rect.y)
        elif window == 'game_main':                                                                             #следование за мышкой
            if self.mouse:
                self.y = pygame.mouse.get_pos()[1] - self.size // 2
                self.x = pygame.mouse.get_pos()[0] - self.size // 2
                self.rect = self.rect.move(self.x - self.rect.x, self.y - self.rect.y)
        return emppuzzle


def game_wreck(pic_name):                                                                                       #отрисовка окна разлома
    screen.fill((64, 62, 115))
    pygame.draw.rect(screen, (30, 0, 0), (50, 50, 500, 500))
    MainPhoto(pic_name)
    main_photo_group.draw(screen)
    show_text(80, "Wreck it!", (171, 141, 210), None, 650, True)
    show_text(40, "« Back to menu", (171, 141, 210), 15, 5, True)


def game_remember(pic_name, kpuzzle, epuzzle, screen_copy_korean, screen_copy):                                 #отрисовка окна запоминания
    pygame.draw.rect(screen, (64, 62, 115), (40, 40, 520, 520))
    main_photo_group.empty()                                                                                    #перерисовка картинки для возвращения на изначальное место после тряски
    pygame.draw.rect(screen, (21, 28, 55), (50, 50, 500, 500))
    MainPhoto(pic_name)
    main_photo_group.draw(screen)
    vowels_korean = ['¹', '»', '½', '¿', 'Á', 'Å', 'Æ', 'Ê', 'Í', 'Ë']                                          #списки букв
    vowels_english = ['a', 'ya', 'eo', 'yeo', 'o', 'yo', 'u', 'yoo', 'ee', 'eu']
    consonants_korean = ['S', 'U', 'V', 'X', 'Y', 'Z', chr(92), '^', '_', 'a', 'b', 'c', 'd', 'e', 'T', 'W', '[', ']',
                         '`']
    consonants_english = ['g', 'n', 'd', 'r, l', 'm', 'b', 'sh', 'ng', 'jh', 'ch', 'kh', 'th', 'ph', 'h', 'kk', 'dd',
                          'bb', 'ss', 'jj']
    diphtongs_korean = ['º', '¼', '¾', 'À', 'Â', 'Ã', 'Ä', 'Ç', 'È', 'É', 'Ì']
    diphtongs_english = ['ae', 'yae', 'e', 'ye', 'wa', 'wae', 'oe', 'wo', 'we', 'wee', 'uee']
    show_text(40, "« Back to menu", (171, 141, 210), 15, 5, True)
    screen_copy_korean = screen.copy()
    screen_copy = screen.copy()
    show_text(80, "Mix it!", (171, 141, 210), None, 650, True)
    if lvl == ['True', 'False', 'False']:                                                                       #расположение клеток гласных
        xy_korean = [[1, 0], [3, 0], [0, 1], [3, 1], [0, 2], [2, 2], [0, 3], [3, 3], [1, 4], [3, 4]]
        ind = random.randint(0, 9)
        for i in xy_korean:
            puz = PuzzleKorean(50 + 100 * i[0], 50 + 100 * i[1], 100, vowels_korean[ind], vowels_english[ind])  #создание случайной корейской клетки
            korean_puzzle_group.add(puz)
            kpuzzle.append(puz)
            vowels_korean.remove(vowels_korean[ind])
            puz = PuzzleEnglish(50 + 100 * (i[0] + 1), 50 + 100 * i[1], 100, vowels_english[ind])               #создание её английского аналога
            english_puzzle_group.add(puz)
            epuzzle.append(puz)
            vowels_english.remove(vowels_english[ind])
            if len(vowels_korean) <= 1:                                                                         #выбор следующей случайной буквы
                ind = 0
            else:
                ind = random.randint(0, len(vowels_korean) - 1)
    elif lvl == ['False', 'True', 'False']:                                                                       #аналогичное расположение клеток согласных
        xy_korean = [[1, 0], [4, 0], [0, 1], [3, 1], [5, 1], [0, 2], [2, 2], [5, 2], [0, 3], [3, 3], [5, 3], [0, 4],
                     [2, 4], [5, 4], [0, 5], [3, 5], [5, 5], [1, 6], [4, 6]]
        ind = random.randint(0, 18)
        for i in xy_korean:
            if i[0] < 4 and i[1] < 4:
                puz = PuzzleKorean(50 + 71 * i[0], 50 + 71 * i[1], 71, consonants_korean[ind], consonants_english[ind])
                korean_puzzle_group.add(puz)
                consonants_korean.remove(consonants_korean[ind])
                kpuzzle.append(puz)
                if i[0] + 1 >= 4:
                    puz = PuzzleEnglish(50 + 71 * 4 + 72 * (i[0] - 3), 50 + 71 * i[1], 72, consonants_english[ind])
                else:
                    puz = PuzzleEnglish(50 + 71 * (i[0] + 1), 50 + 71 * i[1], 71, consonants_english[ind])
                english_puzzle_group.add(puz)
                consonants_english.remove(consonants_english[ind])
                epuzzle.append(puz)
                ind = random.randint(0, len(consonants_korean) - 1)
            elif i[0] >= 4 and i[1] >= 4:
                puz = PuzzleKorean(50 + 71 * 4 + 72 * (i[0] - 4), 50 + 71 * 4 + 72 * (i[1] - 4), 72,
                                   consonants_korean[ind], consonants_english[ind])
                korean_puzzle_group.add(puz)
                consonants_korean.remove(consonants_korean[ind])
                kpuzzle.append(puz)
                puz = PuzzleEnglish(50 + 71 * 4 + 72 * (i[0] - 3), 50 + 71 * 4 + 72 * (i[1] - 4), 72,
                                    consonants_english[ind])
                english_puzzle_group.add(puz)
                consonants_english.remove(consonants_english[ind])
                epuzzle.append(puz)
                if len(consonants_korean) <= 1:
                    ind = 0
                else:
                    ind = random.randint(0, len(consonants_korean) - 1)
            elif i[0] >= 4:
                puz = PuzzleKorean(50 + 71 * 4 + 72 * (i[0] - 4), 50 + 71 * i[1], 72, consonants_korean[ind],
                                   consonants_english[ind])
                korean_puzzle_group.add(puz)
                consonants_korean.remove(consonants_korean[ind])
                kpuzzle.append(puz)
                puz = PuzzleEnglish(50 + 71 * 4 + 72 * (i[0] - 3), 50 + 71 * i[1], 72, consonants_english[ind])
                english_puzzle_group.add(puz)
                consonants_english.remove(consonants_english[ind])
                epuzzle.append(puz)
                ind = random.randint(0, len(consonants_korean) - 1)
            else:
                puz = PuzzleKorean(50 + 71 * i[0], 50 + 71 * 4 + 72 * (i[1] - 4), 72, consonants_korean[ind],
                                   consonants_english[ind])
                korean_puzzle_group.add(puz)
                consonants_korean.remove(consonants_korean[ind])
                kpuzzle.append(puz)
                puz = PuzzleEnglish(50 + 71 * (i[0] + 1), 50 + 71 * 4 + 72 * (i[1] - 4), 72, consonants_english[ind])
                english_puzzle_group.add(puz)
                consonants_english.remove(consonants_english[ind])
                epuzzle.append(puz)
                ind = random.randint(0, len(consonants_korean) - 1)
    elif lvl == ['False', 'False', 'True']:                                                                       #аналогичное расположение клеток дифтонгов
        ind = random.randint(0, 10)
        xy_korean = [[2, 0], [0, 1], [4, 1], [0, 2], [4, 2], [0, 3], [4, 3], [0, 4], [4, 4], [1, 5], [3, 5]]
        for i in xy_korean:
            if i[0] < 4 and i[1] < 4:
                puz = PuzzleKorean(50 + 83 * i[0], 50 + 83 * i[1], 83, diphtongs_korean[ind], diphtongs_english[ind])
                korean_puzzle_group.add(puz)
                diphtongs_korean.remove(diphtongs_korean[ind])
                kpuzzle.append(puz)
                if i[0] + 1 >= 4:
                    puz = PuzzleEnglish(50 + 83 * 4 + 84 * (i[0] - 3), 50 + 83 * i[1], 84, diphtongs_english[ind])
                else:
                    puz = PuzzleEnglish(50 + 83 * (i[0] + 1), 50 + 83 * i[1], 83, diphtongs_english[ind])
                english_puzzle_group.add(puz)
                diphtongs_english.remove(diphtongs_english[ind])
                epuzzle.append(puz)
                ind = random.randint(0, len(diphtongs_korean) - 1)
            elif i[0] >= 4 and i[1] >= 4:
                puz = PuzzleKorean(50 + 83 * 4 + 84 * (i[0] - 4), 50 + 83 * 4 + 84 * (i[1] - 4), 84,
                                   diphtongs_korean[ind], diphtongs_english[ind])
                korean_puzzle_group.add(puz)
                diphtongs_korean.remove(diphtongs_korean[ind])
                kpuzzle.append(puz)
                puz = PuzzleEnglish(50 + 83 * 4 + 84 * (i[0] - 3), 50 + 83 * 4 + 84 * (i[1] - 4), 84,
                                    diphtongs_english[ind])
                english_puzzle_group.add(puz)
                diphtongs_english.remove(diphtongs_english[ind])
                epuzzle.append(puz)
                if len(diphtongs_korean) <= 1:
                    ind = 0
                else:
                    ind = random.randint(0, len(diphtongs_korean) - 1)
            elif i[0] >= 4:
                puz = PuzzleKorean(50 + 83 * 4 + 84 * (i[0] - 4), 50 + 83 * i[1], 84, diphtongs_korean[ind],
                                   diphtongs_english[ind])
                korean_puzzle_group.add(puz)
                diphtongs_korean.remove(diphtongs_korean[ind])
                kpuzzle.append(puz)
                puz = PuzzleEnglish(50 + 83 * 4 + 84 * (i[0] - 3), 50 + 83 * i[1], 84, diphtongs_english[ind])
                english_puzzle_group.add(puz)
                diphtongs_english.remove(diphtongs_english[ind])
                epuzzle.append(puz)
                ind = random.randint(0, len(diphtongs_korean) - 1)
            else:
                puz = PuzzleKorean(50 + 83 * i[0], 50 + 83 * 4 + 84 * (i[1] - 4), 84, diphtongs_korean[ind],
                                   diphtongs_english[ind])
                korean_puzzle_group.add(puz)
                diphtongs_korean.remove(diphtongs_korean[ind])
                kpuzzle.append(puz)
                puz = PuzzleEnglish(50 + 83 * (i[0] + 1), 50 + 83 * 4 + 84 * (i[1] - 4), 84, diphtongs_english[ind])
                english_puzzle_group.add(puz)
                diphtongs_english.remove(diphtongs_english[ind])
                epuzzle.append(puz)
                if len(diphtongs_korean) <= 1:
                    ind = 0
                else:
                    ind = random.randint(0, len(diphtongs_korean) - 1)
    elif lvl == ['True', 'True', 'False']:                                                                       #аналогичное расположение клеток гласных и согласных
        ind = random.randint(0, 28)
        korean = vowels_korean + consonants_korean
        english = vowels_english + consonants_english
        xy_korean = [[2, 0], [4, 0], [6, 0], [0, 1], [2, 1], [4, 1], [6, 1], [0, 2], [2, 2], [4, 2], [6, 2], [0, 3],
                     [2, 3], [4, 3], [6, 3], [0, 4], [2, 4], [4, 4], [6, 4], [0, 5], [2, 5], [4, 5], [6, 5], [0, 6],
                     [2, 6], [4, 6], [0, 7], [2, 7], [4, 7]]
        for i in xy_korean:
            if i[0] < 4 and i[1] < 4:
                puz = PuzzleKorean(50 + 62 * i[0], 50 + 62 * i[1], 62, korean[ind], english[ind])
                korean_puzzle_group.add(puz)
                korean.remove(korean[ind])
                kpuzzle.append(puz)
                if i[0] + 1 >= 4:
                    puz = PuzzleEnglish(50 + 62 * 4 + 63 * (i[0] - 3), 50 + 62 * i[1], 63, english[ind])
                else:
                    puz = PuzzleEnglish(50 + 62 * (i[0] + 1), 50 + 62 * i[1], 62, english[ind])
                english_puzzle_group.add(puz)
                english.remove(english[ind])
                epuzzle.append(puz)
                ind = random.randint(0, len(korean) - 1)
            elif i[0] >= 4 and i[1] >= 4:
                puz = PuzzleKorean(50 + 62 * 4 + 63 * (i[0] - 4), 50 + 62 * 4 + 63 * (i[1] - 4), 63, korean[ind],
                                   english[ind])
                korean_puzzle_group.add(puz)
                korean.remove(korean[ind])
                kpuzzle.append(puz)
                puz = PuzzleEnglish(50 + 62 * 4 + 63 * (i[0] - 3), 50 + 62 * 4 + 63 * (i[1] - 4), 63, english[ind])
                english_puzzle_group.add(puz)
                english.remove(english[ind])
                epuzzle.append(puz)
                if len(korean) <= 1:
                    ind = 0
                else:
                    ind = random.randint(0, len(korean) - 1)
            elif i[0] >= 4:
                puz = PuzzleKorean(50 + 62 * 4 + 63 * (i[0] - 4), 50 + 62 * i[1], 63, korean[ind], english[ind])
                korean_puzzle_group.add(puz)
                korean.remove(korean[ind])
                kpuzzle.append(puz)
                puz = PuzzleEnglish(50 + 62 * 4 + 63 * (i[0] - 3), 50 + 62 * i[1], 63, english[ind])
                english_puzzle_group.add(puz)
                english.remove(english[ind])
                epuzzle.append(puz)
                ind = random.randint(0, len(korean) - 1)
            else:
                puz = PuzzleKorean(50 + 62 * i[0], 50 + 62 * 4 + 63 * (i[1] - 4), 63, korean[ind], english[ind])
                korean_puzzle_group.add(puz)
                korean.remove(korean[ind])
                kpuzzle.append(puz)
                puz = PuzzleEnglish(50 + 62 * (i[0] + 1), 50 + 62 * 4 + 63 * (i[1] - 4), 63, english[ind])
                english_puzzle_group.add(puz)
                english.remove(english[ind])
                epuzzle.append(puz)
                ind = random.randint(0, len(korean) - 1)
    elif lvl == ['False', 'True', 'True']:                                                                       #аналогичное расположение клеток согласных и дифтонгов
        ind = random.randint(0, 29)
        korean = consonants_korean + diphtongs_korean
        english = consonants_english + diphtongs_english
        xy_korean = [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7], [2, 0], [2, 1], [2, 2], [2, 4],
                     [2, 5], [2, 6], [2, 7], [4, 0], [4, 1], [4, 2], [4, 3], [4, 5], [4, 6], [4, 7], [6, 0], [6, 1],
                     [6, 2], [6, 3], [6, 4], [6, 5], [6, 6], [6, 7]]
        for i in xy_korean:
            if i[0] < 4 and i[1] < 4:
                puz = PuzzleKorean(50 + 62 * i[0], 50 + 62 * i[1], 62, korean[ind], english[ind])
                korean_puzzle_group.add(puz)
                korean.remove(korean[ind])
                kpuzzle.append(puz)
                puz = PuzzleEnglish(50 + 62 * (i[0] + 1), 50 + 62 * i[1], 62, english[ind])
                english_puzzle_group.add(puz)
                english.remove(english[ind])
                epuzzle.append(puz)
                ind = random.randint(0, len(korean) - 1)
            elif i[0] >= 4 and i[1] >= 4:
                puz = PuzzleKorean(50 + 62 * 4 + 63 * (i[0] - 4), 50 + 62 * 4 + 63 * (i[1] - 4), 63, korean[ind],
                                   english[ind])
                korean_puzzle_group.add(puz)
                korean.remove(korean[ind])
                kpuzzle.append(puz)
                puz = PuzzleEnglish(50 + 62 * 4 + 63 * (i[0] - 3), 50 + 62 * 4 + 63 * (i[1] - 4), 63, english[ind])
                english_puzzle_group.add(puz)
                english.remove(english[ind])
                epuzzle.append(puz)
                if len(korean) <= 1:
                    ind = 0
                else:
                    ind = random.randint(0, len(korean) - 1)
            elif i[0] >= 4:
                puz = PuzzleKorean(50 + 62 * 4 + 63 * (i[0] - 4), 50 + 62 * i[1], 63, korean[ind], english[ind])
                korean_puzzle_group.add(puz)
                korean.remove(korean[ind])
                kpuzzle.append(puz)
                puz = PuzzleEnglish(50 + 62 * 4 + 63 * (i[0] - 3), 50 + 62 * i[1], 63, english[ind])
                english_puzzle_group.add(puz)
                english.remove(english[ind])
                epuzzle.append(puz)
                ind = random.randint(0, len(korean) - 1)
            else:
                puz = PuzzleKorean(50 + 62 * i[0], 50 + 62 * 4 + 63 * (i[1] - 4), 63, korean[ind], english[ind])
                korean_puzzle_group.add(puz)
                korean.remove(korean[ind])
                kpuzzle.append(puz)
                puz = PuzzleEnglish(50 + 62 * (i[0] + 1), 50 + 62 * 4 + 63 * (i[1] - 4), 63, english[ind])
                english_puzzle_group.add(puz)
                english.remove(english[ind])
                epuzzle.append(puz)
                ind = random.randint(0, len(korean) - 1)
    elif lvl == ['True', 'False', 'True']:                                                                       #аналогичное расположение клеток гласных и дифтонгов
        ind = random.randint(0, 20)
        korean = vowels_korean + diphtongs_korean
        english = vowels_english + diphtongs_english
        xy_korean = [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [2, 0], [2, 2], [2, 4], [2, 6], [3, 1],
                     [3, 3], [3, 5], [5, 0], [5, 1], [5, 2], [5, 3], [5, 4], [5, 5], [5, 6]]
        for i in xy_korean:
            if i[0] < 4 and i[1] < 4:
                puz = PuzzleKorean(50 + 71 * i[0], 50 + 71 * i[1], 71, korean[ind], english[ind])
                korean_puzzle_group.add(puz)
                kpuzzle.append(puz)
                if i[0] + 1 >= 4:
                    puz = PuzzleEnglish(50 + 71 * 4 + 72 * (i[0] - 3), 50 + 71 * i[1], 72, english[ind])
                else:
                    puz = PuzzleEnglish(50 + 71 * (i[0] + 1), 50 + 71 * i[1], 71, english[ind])
                english_puzzle_group.add(puz)
                epuzzle.append(puz)
                korean.remove(korean[ind])
                english.remove(english[ind])
                ind = random.randint(0, len(korean) - 1)
            elif i[0] >= 4 and i[1] >= 4:
                puz = PuzzleKorean(50 + 71 * 4 + 72 * (i[0] - 4), 50 + 71 * 4 + 72 * (i[1] - 4), 72, korean[ind],
                                   english[ind])
                korean_puzzle_group.add(puz)
                kpuzzle.append(puz)
                puz = PuzzleEnglish(50 + 71 * 4 + 72 * (i[0] - 3), 50 + 71 * 4 + 72 * (i[1] - 4), 72, english[ind])
                english_puzzle_group.add(puz)
                epuzzle.append(puz)
                korean.remove(korean[ind])
                english.remove(english[ind])
                if len(korean) <= 1:
                    ind = 0
                else:
                    ind = random.randint(0, len(korean) - 1)
            elif i[0] >= 4:
                puz = PuzzleKorean(50 + 71 * 4 + 72 * (i[0] - 4), 50 + 71 * i[1], 72, korean[ind], english[ind])
                korean_puzzle_group.add(puz)
                kpuzzle.append(puz)
                puz = PuzzleEnglish(50 + 71 * 4 + 72 * (i[0] - 3), 50 + 71 * i[1], 72, english[ind])
                english_puzzle_group.add(puz)
                epuzzle.append(puz)
                korean.remove(korean[ind])
                english.remove(english[ind])
                ind = random.randint(0, len(korean) - 1)
            else:
                puz = PuzzleKorean(50 + 71 * i[0], 50 + 71 * 4 + 72 * (i[1] - 4), 72, korean[ind], english[ind])
                korean_puzzle_group.add(puz)
                kpuzzle.append(puz)
                puz = PuzzleEnglish(50 + 71 * (i[0] + 1), 50 + 71 * 4 + 72 * (i[1] - 4), 72, english[ind])
                english_puzzle_group.add(puz)
                epuzzle.append(puz)
                korean.remove(korean[ind])
                english.remove(english[ind])
                ind = random.randint(0, len(korean) - 1)
    elif lvl == ['True', 'True', 'True']:                                                                       #аналогичное расположение клеток гласных, согласных и дифтонгов
        ind = random.randint(0, 39)
        korean = vowels_korean + consonants_korean + diphtongs_korean
        english = vowels_english + consonants_english + diphtongs_english
        xy_korean = [[0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7], [0, 8], [2, 0], [2, 2], [2, 3], [2, 4],
                     [2, 5], [2, 6], [2, 7], [2, 9], [4, 0], [4, 1], [4, 3], [4, 4], [4, 5], [4, 6], [4, 8], [4, 9],
                     [6, 0], [6, 1], [6, 2], [6, 4], [6, 5], [6, 7], [6, 8], [6, 9], [8, 0], [8, 1], [8, 2], [8, 3],
                     [8, 6], [8, 7], [8, 8], [8, 9]]
        for i in xy_korean:
            puz = PuzzleKorean(50 + 50 * i[0], 50 + 50 * i[1], 50, korean[ind], english[ind])
            korean_puzzle_group.add(puz)
            kpuzzle.append(puz)
            puz = PuzzleEnglish(50 + 50 * (i[0] + 1), 50 + 50 * i[1], 50, english[ind])
            english_puzzle_group.add(puz)
            epuzzle.append(puz)
            korean.remove(korean[ind])
            english.remove(english[ind])
            if len(korean) <= 1:
                ind = 0
            else:
                ind = random.randint(0, len(korean) - 1)
    korean_puzzle_group.draw(screen)                                                                            #отрисовка
    korean_puzzle_group.draw(screen_copy_korean)
    english_puzzle_group.draw(screen)
    epuzzle.reverse()                                                                                           #перевёрнутый массив, чтобы нажимать на верхние клетки
    return [kpuzzle, epuzzle, screen_copy_korean, screen_copy]


def game_main(emppuzzle):                                                                                       #отрисовка главного окна игры
    text = show_text(40, "Check it!", (171, 141, 210), 480, 5, True)
    w, h = text.get_width(), text.get_height()
    screen_copy_korean.blit(text, (480, 5))
    pygame.draw.rect(screen_copy_korean, (171, 141, 210), (480 - 8, 5, w + 20, h - 15), 1)
    screen_copy.blit(text, (480, 5))
    pygame.draw.rect(screen_copy, (171, 141, 210), (480 - 8, 5, w + 20, h - 15), 1)
    for i in epuzzle:                                                                                           #создание пустых клеток и перемещение английских на случайные места
        emppuzzle = i.update("game_rem", emppuzzle)
    english_puzzle_group.draw(screen)
    return emppuzzle


def warning(warning_copy, question, describtion):                                                               #отрисовка окон предупреждения с кнопками Yes и No
    warning_copy = screen.copy()                                                                                #охранение последнего холста
    fullname = os.path.join('data', 'black.png')                                                                #путь к затемнению
    screen.blit(pygame.image.load(fullname).convert_alpha(), (0, 0))
    pygame.draw.rect(screen, (64, 62, 115), (40, 200, 520, 300))
    show_text(80, "Warning!", (171, 141, 210), None, 220, False)
    show_text(40, question, (171, 141, 210), None, 300, False)
    show_text(40, describtion, (171, 141, 210), None, 340, False)
    show_btn_yes((171, 141, 210))
    show_btn_no((171, 141, 210))
    return warning_copy


def warning_choosing(warning_copy):                                                                             #отрисовка окна предупреждения обязательного выбора в настройках
    warning_copy = screen.copy()                                                                                #охранение последнего холста
    fullname = os.path.join('data', 'black.png')                                                                #путь к затемнению
    screen.blit(pygame.image.load(fullname).convert_alpha(), (0, 0))
    pygame.draw.rect(screen, (64, 62, 115), (20, 200, 560, 280))
    show_text(80, "Warning!", (171, 141, 210), None, 220, False)
    show_text(40, "Please choose at least one part of the alphabet.", (171, 141, 210), None, 300, False)
    show_text(80, "Ok", (171, 141, 210), None, 360, True)
    return warning_copy


if __name__ == '__main__':
    pygame.init()
    main_photo_group = pygame.sprite.Group()                                                            #объявление групп спрайтов
    korean_puzzle_group = pygame.sprite.Group()
    english_puzzle_group = pygame.sprite.Group()
    empty_puzzle_group = pygame.sprite.Group()
    kpuzzle = []                                                                                        #объявление массивов спрайтов
    epuzzle = []
    emppuzzle = []
    touch = None                                                                                        #нажатый паззл
    clock = pygame.time.Clock()
    speed = 1
    ticks = 0
    btn_settings, btn_save, btn_lp, btn_btm = False, False, False, False                                #состоянии кнопок: Settings, Save, Let's play, Back to menu
    btn_yes, btn_no, btn_ok = False, False, False                                                       #Yes, No, Ok
    btn_wi, btn_mi, btn_ci = False, False, False                                                        #Wreak it, Mix it, Check it
    size = width, height = 600, 800
    screen = pygame.display.set_mode(size)
    screen.fill((64, 62, 115))
    screen_copy_korean = screen.copy()                                                                  #создание копий холстов
    screen_copy = screen.copy()
    warning_copy = screen.copy()
    window = 'main'
    main_window()
    running = True                                                                                      #запуск игрового цикла
    lvl = ["True", "False", "False"]                                                                    #уровни по умолчанию
    lvl_copy = lvl.copy()                                                                               #копия уровней
    pic_name = "skz.jpg"                                                                                #картинка по умолчанию
    pic_name_copy = pic_name                                                                            #копия картинки на фоне
    while running:                                                                                      #игровой цикл
        for event in pygame.event.get():                                                                #если совершаются события
            if window == 'main':                                                                        #нажатия в главном окне
                if event.type == pygame.MOUSEBUTTONDOWN and 177 <= event.pos[0] <= 423 and 364 <= \
                        event.pos[1] <= 447:                                                            #Settings нажали
                    show_text(80, "Settings", (229, 186, 237), None, 370, True)
                    btn_settings = True
                elif event.type == pygame.MOUSEBUTTONUP and btn_settings:                               #Settings отпустили
                    btn_settings = False
                    show_text(80, "Settings", (171, 141, 210), None, 370, True)
                    window = 'settings'
                    settings(lvl, pic_name)
                elif event.type == pygame.MOUSEBUTTONDOWN and 179 <= event.pos[0] <= 421 and 644 <= \
                        event.pos[1] <= 728:                                                            #Let's play нажали
                    show_text(80, "Let's play!", (229, 186, 237), None, 650, True)
                    btn_lp = True
                elif event.type == pygame.MOUSEBUTTONUP and btn_lp:                                     #Let's play отпустили
                    btn_lp = False
                    show_text(80, "Let's play!", (171, 141, 210), None, 650, True)
                    window = 'game_wreck'
                    game_wreck(pic_name)
            elif window == 'settings':                                                                  #нажатия в окне настроек
                if event.type == pygame.MOUSEBUTTONDOWN and 10 <= event.pos[0] <= 190 and \
                        9 <= event.pos[1] <= 41:                                                        #Back to menu нажали
                    pygame.draw.rect(screen_copy_korean, (64, 62, 115), (10, 9, 180, 32))
                    show_text(40, "« Back to menu", (229, 186, 237), 15, 5, True)
                    btn_btm = True
                elif event.type == pygame.MOUSEBUTTONUP and btn_btm:                                    #Back to menu отпустили
                    show_text(40, "« Back to menu", (171, 141, 210), 15, 5, True)
                    btn_btm = False
                    window = 'not_saved'
                    warning_copy = warning(warning_copy, "Are you sure you want to go back to menu?", "Changes will not be saved.")
                elif event.type == pygame.MOUSEBUTTONDOWN and 220 <= event.pos[0] <= 235 and \
                        235 <= event.pos[1] <= 250:                                                     #volwes нажали
                    if lvl[0] == 'False':                                                               #Гласные не были выбраны изначально
                        pygame.draw.rect(screen, (229, 186, 237), (225, 240, 5, 5))
                        pygame.draw.rect(screen, (229, 186, 237), (220, 235, 15, 15), 1)
                        lvl[0] = 'True'
                    else:                                                                               #Гласные были выбраны изначально
                        pygame.draw.rect(screen, (64, 62, 115), (219, 234, 17, 17))
                        pygame.draw.rect(screen, (171, 141, 210), (220, 235, 15, 15), 1)
                        lvl[0] = 'False'
                elif event.type == pygame.MOUSEBUTTONDOWN and 220 <= event.pos[0] <= 235 and \
                        275 <= event.pos[1] <= 290:                                                     #consonants нажали
                    if lvl[1] == 'False':                                                               #Согласные не были выбраны изначально
                        pygame.draw.rect(screen, (229, 186, 237), (225, 280, 5, 5))
                        pygame.draw.rect(screen, (229, 186, 237), (220, 275, 15, 15), 1)
                        lvl[1] = 'True'
                    else:                                                                               #Согласные были выбраны изначально
                        pygame.draw.rect(screen, (64, 62, 115), (219, 274, 17, 17))
                        pygame.draw.rect(screen, (171, 141, 210), (220, 275, 15, 15), 1)
                        lvl[1] = 'False'
                elif event.type == pygame.MOUSEBUTTONDOWN and 220 <= event.pos[0] <= 235 and \
                        315 <= event.pos[1] <= 330:                                                     #diftongs нажали
                    if lvl[2] == 'False':                                                               #Дифтонги не были выбраны изначально
                        pygame.draw.rect(screen, (229, 186, 237), (225, 320, 5, 5))
                        pygame.draw.rect(screen, (229, 186, 237), (220, 315, 15, 15), 1)
                        lvl[2] = 'True'
                    else:                                                                               #Дифтонги были выбраны изначально
                        pygame.draw.rect(screen, (64, 62, 115), (219, 314, 17, 17))
                        pygame.draw.rect(screen, (171, 141, 210), (220, 315, 15, 15), 1)
                        lvl[2] = 'False'
                elif event.type == pygame.MOUSEBUTTONDOWN and 218 <= event.pos[0] <= 236 and \
                        481 <= event.pos[1] <= 499:                                                     #выбрали фон bts
                    if pic_name != "bts.jpg":                                                           #если до этого не выбраны
                        pygame.draw.rect(screen, (64, 62, 115), (217, 480, 20, 20))                     #перерисовка: bts выбраны
                        pygame.draw.circle(screen, (229, 186, 237), (227, 490), 4)
                        pygame.draw.circle(screen, (229, 186, 237), (227, 490), 9, 1)
                        pygame.draw.rect(screen, (64, 62, 115), (217, 520, 20, 20))                     #перерисовка: остальные не выбраны
                        pygame.draw.circle(screen, (171, 141, 210), (227, 530), 9, 1)
                        pygame.draw.rect(screen, (64, 62, 115), (217, 560, 20, 20))
                        pygame.draw.circle(screen, (171, 141, 210), (227, 570), 9, 1)
                        pic_name = 'bts.jpg'
                elif event.type == pygame.MOUSEBUTTONDOWN and 218 <= event.pos[0] <= 236 and \
                        521 <= event.pos[1] <= 539:                                                     #выбрали фон skz
                    if pic_name != "skz.jpg":                                                           #если до этого не выбраны
                        pygame.draw.rect(screen, (64, 62, 115), (217, 520, 20, 20))                     #перерисовка: skz выбраны
                        pygame.draw.circle(screen, (229, 186, 237), (227, 530), 4)
                        pygame.draw.circle(screen, (229, 186, 237), (227, 530), 9, 1)
                        pygame.draw.rect(screen, (64, 62, 115), (217, 480, 20, 20))                     #перерисовка: остальные не выбраны
                        pygame.draw.circle(screen, (171, 141, 210), (227, 490), 9, 1)
                        pygame.draw.rect(screen, (64, 62, 115), (217, 560, 20, 20))
                        pygame.draw.circle(screen, (171, 141, 210), (227, 570), 9, 1)
                        pic_name = 'skz.jpg'
                elif event.type == pygame.MOUSEBUTTONDOWN and 218 <= event.pos[0] <= 236 and \
                        561 <= event.pos[1] <= 579:                                                     #выбрали фон txt
                    if pic_name != "txt.jpg":                                                           #если до этого не выбраны
                        pygame.draw.rect(screen, (64, 62, 115), (217, 560, 20, 20))                     #перерисовка: txt выбраны
                        pygame.draw.circle(screen, (229, 186, 237), (227, 570), 4)
                        pygame.draw.circle(screen, (229, 186, 237), (227, 570), 9, 1)
                        pygame.draw.rect(screen, (64, 62, 115), (217, 480, 20, 20))                     #перерисовка: остальные не выбраны
                        pygame.draw.circle(screen, (171, 141, 210), (227, 490), 9, 1)
                        pygame.draw.rect(screen, (64, 62, 115), (217, 520, 20, 20))
                        pygame.draw.circle(screen, (171, 141, 210), (227, 530), 9, 1)
                        pic_name = 'txt.jpg'
                elif event.type == pygame.MOUSEBUTTONDOWN and 226 <= event.pos[0] <= 364 and \
                        650 <= event.pos[1] <= 723:                                                     #Save нажали
                    pygame.draw.rect(screen, (64, 62, 115), (226, 650, 138, 73))
                    show_text(80, "SAVE", (229, 186, 237), None, 650, True)
                    btn_save = True
                elif event.type == pygame.MOUSEBUTTONUP and btn_save:                                   #Save отпустили
                    show_text(80, "SAVE", (171, 141, 210), None, 650, True)
                    btn_save = False
                    if lvl == ['False', 'False', 'False']:                                              #Проверка: выбран ли хоть один уровень
                        window = 'not_chosen'
                        warning_copy = warning_choosing(warning_copy)
                    else:
                        lvl_copy = lvl.copy()
                        pic_name_copy = pic_name
                        window = 'main'
                        main_window()
            elif window == 'game_wreck':                                                                #нажатия в окне разлома
                if event.type == pygame.MOUSEBUTTONDOWN and 188 <= event.pos[0] <= 412 and \
                        650 <= event.pos[1] <= 723:                                                     #Wreck it нажали
                    pygame.draw.rect(screen, (64, 62, 115), (188, 650, 224, 73))
                    show_text(80, "Wreck it!", (229, 186, 237),  None, 650, True)
                    btn_wi = True
                elif event.type == pygame.MOUSEBUTTONUP and btn_wi:                                     #Wreck it отпустили
                    show_text(80, "Wreck it!", (171, 141, 210), None, 650, True)
                    btn_wi = False
                    start_time = time.time()
                    window = 'shaking'
                elif event.type == pygame.MOUSEBUTTONDOWN and 10 <= event.pos[0] <= 190 and \
                        9 <= event.pos[1] <= 41:                                                        #Back to menu нажали
                    pygame.draw.rect(screen_copy_korean, (64, 62, 115), (10, 9, 180, 32))
                    show_text(40, "« Back to menu", (229, 186, 237), 15, 5, True)
                    btn_btm = True
                elif event.type == pygame.MOUSEBUTTONUP and btn_btm:                                    #Back to menu отпустили
                    show_text(40, "« Back to menu", (171, 141, 210), 15, 5, True)
                    btn_btm = False
                    window = 'main'
                    main_window()
            elif window == 'game_rem':                                                                  #нажатия в окне запоминания
                if event.type == pygame.MOUSEBUTTONDOWN and 10 <= event.pos[0] <= 190 and \
                        9 <= event.pos[1] <= 41:                                                        #Back to menu нажали
                    pygame.draw.rect(screen_copy_korean, (64, 62, 115), (10, 9, 180, 32))
                    show_text(40, "« Back to menu", (229, 186, 237), 15, 5, True)
                    btn_btm = True
                elif event.type == pygame.MOUSEBUTTONUP and btn_btm:                                    #Back to menu отпустили
                    show_text(40, "« Back to menu", (171, 141, 210), 15, 5, True)
                    btn_btm = False
                    window = 'main'
                    english_puzzle_group.empty()                                                        #очистка частей паззла
                    korean_puzzle_group.empty()
                    main_photo_group.empty()
                    empty_puzzle_group.empty()
                    emppuzzle = []
                    kpuzzle = []
                    epuzzle = []
                    main_window()
                elif event.type == pygame.MOUSEBUTTONDOWN and 215 <= event.pos[0] <= 385 and \
                        650 <= event.pos[1] <= 723:                                                     #Mix it нажали
                    show_text(80, "Mix it!", (229, 186, 237), None, 650, True)
                    btn_mi = True
                elif event.type == pygame.MOUSEBUTTONUP and btn_mi:                                     #Mix it отпустили
                    show_text(80, "Mix it!", (171, 141, 210), None, 650, True)
                    btn_mi = False
                    game_main(emppuzzle)
                    window = 'game_main'
            elif window == 'game_main':                                                                 #нажатия в главном окне игры
                if event.type == pygame.MOUSEBUTTONDOWN and 10 <= event.pos[0] <= 190 and \
                        9 <= event.pos[1] <= 41:  # Back to menu нажали
                    pygame.draw.rect(screen_copy_korean, (64, 62, 115), (10, 9, 180, 32))
                    text = pygame.font.Font('moolbor.ttf', 40).render("« Back to menu", 1, (229, 186, 237))
                    screen_copy_korean.blit(text, (15, 5))
                    w, h = text.get_width(), text.get_height()
                    pygame.draw.rect(screen_copy_korean, (229, 186, 237), (15 - 8, 5, w + 20, h - 15), 1)
                    btn_btm = True
                elif event.type == pygame.MOUSEBUTTONUP and btn_btm:  # Back to menu отпустили
                    text = pygame.font.Font('moolbor.ttf', 40).render("« Back to menu", 1, (171, 141, 210))
                    screen_copy_korean.blit(text, (15, 5))
                    w, h = text.get_width(), text.get_height()
                    pygame.draw.rect(screen_copy_korean, (171, 141, 210), (15 - 8, 5, w + 20, h - 15), 1)
                    btn_btm = False
                    window = "exit_game"
                    warning_copy = warning(warning_copy, "Are you sure you want to go back to menu?", "Game will not be saved.")
                elif event.type == pygame.MOUSEBUTTONDOWN and 475 <= event.pos[0] <= 585 and \
                        8 <= event.pos[1] <= 42:                                                        #Check it нажали
                    pygame.draw.rect(screen_copy_korean, (64, 62, 115), (470, 3, 120, 44))
                    text = pygame.font.Font('moolbor.ttf', 40).render("Check it!", 1, (229, 186, 237))
                    screen_copy_korean.blit(text, (480, 5))
                    w, h = text.get_width(), text.get_height()
                    pygame.draw.rect(screen_copy_korean, (229, 186, 237), (480 - 8, 5, w + 20, h - 15), 1)
                    btn_ci = True
                elif event.type == pygame.MOUSEBUTTONUP and btn_ci:                                     #Check it отпустили
                    text = pygame.font.Font('moolbor.ttf', 40).render("Check it!", 1, (171, 141, 210))
                    screen_copy_korean.blit(text, (480, 5))
                    w, h = text.get_width(), text.get_height()
                    pygame.draw.rect(screen_copy_korean, (171, 141, 210), (480 - 8, 5, w + 20, h - 15), 1)
                    btn_ci = False
                    delete = []
                    for i in kpuzzle:
                        for j in epuzzle:
                            if abs(j.rect[1] - i.rect[1]) <= 3 and abs(
                                    j.rect[0] - j.size - i.rect[0]) <= 3 and i.check == j.check:        #проверка правильности позиций
                                delete.append(i)
                                i.kill()                                                                #удаление корейской клетки
                                for e in emppuzzle:
                                    if e.rect == j.rect:                                                #нахождение пустой клетки под английской
                                        delete.append(e)
                                        e.kill()                                                        #удаление пустой клетки
                                        break
                                delete.append(j)
                                j.kill()                                                                #удаление английской клетки
                                break
                    for i in delete:                                                                    #удаление из массивов
                        if i in kpuzzle:
                            kpuzzle.remove(i)
                        elif i in epuzzle:
                            epuzzle.remove(i)
                        elif i in emppuzzle:
                            emppuzzle.remove(i)
                    screen_copy_korean.blit(screen_copy, (0, 0))                                        #отрисовка на холстах
                    korean_puzzle_group.draw(screen_copy_korean)
                    empty_puzzle_group.draw(screen_copy_korean)
                    screen.blit(screen_copy_korean, (0, 0))
                    english_puzzle_group.draw(screen)
                    if len(kpuzzle) == 0:                                                               #если всё решено
                        window = 'good_job'
                        show_text(80, "Good job!", (171, 141, 210), None, 650, False)
                elif event.type == pygame.MOUSEBUTTONDOWN and not touch:                                #проверка нажатия на клетки
                    for i in epuzzle:
                        if i.rect[0] <= event.pos[0] <= i.rect[0] + i.rect[2] and \
                                i.rect[1] <= event.pos[1] <= i.rect[1] + i.rect[3]:                     #проверка координат
                            touch = i                                                                   #выделение нажатой клетки
                            i.kill()
                            english_puzzle_group.add(touch)
                            touch.mouse = True
                            break
                elif event.type == pygame.MOUSEBUTTONDOWN and touch.mouse:                              #если клетку отпустили
                    touch.mouse = False
                    for i in emppuzzle:                                                                 #если координаты пустой клетки совпадают с английской
                        if abs(i.rect[0] - touch.rect[0]) <= 10 and \
                                abs(i.rect[1] - touch.rect[1]) <= 20:
                            touch.rect = i.rect                                                         #поставить английскую клетку на место пустой
                    for i in epuzzle:                                                                   #если координаты клетки совпадают с другой
                        if i.rect == touch.rect and i != touch:
                            touch.mouse = True                                                          #продолжить перемещение
                            break
                    else:
                        touch = None                                                                    #иначе перемещение закончить
            elif window == 'good_job':                                                                  #нажатия в окне завершения игры
                if event.type == pygame.MOUSEBUTTONDOWN and 10 <= event.pos[0] <= 190 and \
                        9 <= event.pos[1] <= 41:                                                        #Back to menu нажали
                    show_text(40, "« Back to menu", (229, 186, 237), 15, 5, True)
                    btn_btm = True
                elif event.type == pygame.MOUSEBUTTONUP and btn_btm:                                    #Back to menu отпустили
                    show_text(40, "« Back to menu", (171, 141, 210), 15, 5, True)
                    btn_btm = False
                    window = 'main'
                    main_window()
            elif window == 'not_saved':                                                                 #нажатия в окне предупреждения несохранения настроек
                if event.type == pygame.MOUSEBUTTONDOWN and 206 <= event.pos[0] <= 294 and \
                        408 <= event.pos[1] <= 471:                                                     #Yes нажали
                    show_btn_yes((229, 186, 237))
                    btn_yes = True
                elif event.type == pygame.MOUSEBUTTONUP and btn_yes:                                    #Yes отпустили
                    show_btn_yes((171, 141, 210))
                    btn_yes = False
                    lvl = lvl_copy.copy()
                    pic_name = pic_name_copy
                    window = 'main'
                    main_window()
                elif event.type == pygame.MOUSEBUTTONDOWN and 321 <= event.pos[0] <= 399 and \
                        408 <= event.pos[1] <= 471:                                                     #No нажали
                    show_btn_no((229, 186, 237))
                    btn_no = True
                elif event.type == pygame.MOUSEBUTTONUP and btn_no:                                     #No отпустили
                    show_btn_no((171, 141, 210))
                    btn_no = False
                    screen.blit(warning_copy, (0, 0))                                                   #отрисовка последнего холста
                    window = 'settings'
            elif window == "exit_game":                                                                 #нажатия окна предупреждения выхода их игры
                if event.type == pygame.MOUSEBUTTONDOWN and 206 <= event.pos[0] <= 294 and \
                        408 <= event.pos[1] <= 471:                                                     #Yes нажали
                    show_btn_yes((229, 186, 237))
                    btn_yes = True
                elif event.type == pygame.MOUSEBUTTONUP and btn_yes:                                    #Yes отпустили
                    show_btn_yes((171, 141, 210))
                    btn_yes = False
                    english_puzzle_group.empty()                                                        #очистка всех частей паззла
                    korean_puzzle_group.empty()
                    main_photo_group.empty()
                    empty_puzzle_group.empty()
                    emppuzzle = []
                    kpuzzle = []
                    epuzzle = []
                    window = 'main'
                    main_window()
                elif event.type == pygame.MOUSEBUTTONDOWN and 321 <= event.pos[0] <= 399 and \
                        408 <= event.pos[1] <= 471:                                                     #No нажали
                    show_btn_no((229, 186, 237))
                    btn_no = True
                elif event.type == pygame.MOUSEBUTTONUP and btn_no:                                     #No отпустили
                    show_btn_no((171, 141, 210))
                    btn_no = False
                    screen.blit(warning_copy,(0,0))                                                 #отрисовка последнего холста
                    window = "game_main"
            elif window == 'not_chosen':                                                                #нажатия в окне обязательного выбора уровня
                if event.type == pygame.MOUSEBUTTONDOWN and 261 <= event.pos[0] <= 340 and \
                        368 <= event.pos[1] <= 431:                                                     #Ok нажали
                    show_text(80, "Ok", (229, 186, 237), None, 360, True)
                    btn_ok = True
                elif event.type == pygame.MOUSEBUTTONUP and btn_ok:                                     #Ok отпустили
                    show_text(80, "Ok", (171, 141, 210), None, 360, True)
                    btn_ok = False
                    screen.blit(warning_copy, (0, 0))                                                   #отрисовка последнего холста
                    window = 'settings'
            if event.type == pygame.QUIT:                                                               #выход из цикла игры
                running = False
        if window == 'shaking':                                                                         #окно тряски перед разломом
            screen.fill((64, 62, 115))
            pygame.draw.rect(screen, (21, 28, 55), (50, 50, 500, 500))
            main_photo_group.update()
            main_photo_group.draw(screen)
            if time.time() - start_time >= 1.5:                                                         #по завершении перейти в окно запоминания
                window = 'game_rem'
                responde = game_remember(pic_name, kpuzzle, epuzzle, screen_copy_korean, screen_copy)
                kpuzzle, epuzzle, screen_copy_korean, screen_copy = responde[0], responde[1], \
                                                                    responde[2], responde[3]
        elif window == 'game_main':                                                                     #перемещение клеток в главном окне игры
            screen.blit(screen_copy_korean, (0, 0))
            for i in epuzzle:
                emppuzzle = i.update("game_main", emppuzzle)
            english_puzzle_group.draw(screen)
        pygame.display.flip()
    pygame.quit()
